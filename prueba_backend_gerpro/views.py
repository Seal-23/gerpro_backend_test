from django.http import JsonResponse
import json
import pandas as pd


def case1(request,inicio,fin,paso):
    df = pd.read_csv(r'datos_prueba_tecnica.csv', sep=';')
    filter_df = df[(df.Inicio >= inicio) & (df.Fin <= fin)]
    iri_array = []
    for begin_of_range in range(inicio, fin, paso):
        end_of_range = begin_of_range + paso
        zones_to_consider = filter_df[~((filter_df.Fin <= begin_of_range) | (filter_df.Inicio >= (end_of_range)))]
        iri=0
        for row in zones_to_consider.axes[0]:
          weighting_factor=0
          if begin_of_range<zones_to_consider.at[row,'Inicio'] and end_of_range<zones_to_consider.at[row,'Fin']:
            weighting_factor=end_of_range-zones_to_consider.at[row,'Inicio']
          if begin_of_range>=zones_to_consider.at[row,'Inicio'] and end_of_range<=zones_to_consider.at[row,'Fin']:
            weighting_factor=paso
          if begin_of_range>zones_to_consider.at[row,'Inicio'] and end_of_range>zones_to_consider.at[row,'Fin']:
            weighting_factor=zones_to_consider.at[row,'Fin']-begin_of_range
          if begin_of_range<=zones_to_consider.at[row,'Inicio'] and end_of_range>=zones_to_consider.at[row,'Fin']:
            weighting_factor=zones_to_consider.at[row,'Fin']-zones_to_consider.at[row,'Inicio']
          iri+=zones_to_consider.at[row,'IRI']*weighting_factor
        iri_array.append({
            "inicio": begin_of_range,
            "fin": end_of_range,
            "iri": iri/paso
        })

    return JsonResponse(iri_array, safe=False)
